var nodesArray = [],
    edgeArray = [],
    resultFile = {},
    s;


document.getElementById('addW').addEventListener('click', function () {
    var node = {};
    node.id = document.getElementById('idW').value;
    node.label = document.getElementById('lab').value;
    node.x = document.getElementById('pozX').value;
    node.y = document.getElementById('pozY').value;
    node.size = document.getElementById('size').value;


    var canIAddNode = true; // flaga czy mogę dodać węzeł
    for (var i = 0; i < nodesArray.length; i++) { // iterujemy przez el
        if (nodesArray[i].id == node.id) { // jeśli trafimy na el. o tym samym id
            canIAddNode = false; // zaznaczamy że nie można dodać nowego węzła
            break;
        }
    }
    if (canIAddNode) { // to przejdzie tylko jeśli sprawdzenie
        nodesArray.push(node); // wyżej nie napotkało węzła o tym samym id
        console.log(nodesArray);
        rysujGraf();
    } else {
        alert('Wierzchołek ' + document.getElementById('idW').value + ' już istnieje');
    }


});

document.getElementById('addK').addEventListener('click', function () {
    var edge = {};
    edge.id = document.getElementById('idK').value;
    edge.source = document.getElementById('fromW').value;
    edge.target = document.getElementById('toW').value;

    var canIAddEdge = true; // flaga czy mogę dodać węzeł
    for (var i = 0; i < edgeArray.length; i++) { // iterujemy przez el
        if (edgeArray[i].id == edge.id) { // jeśli trafimy na el. o tym samym id
            canIAddEdge = false;
            break;
        }
    }
    if (canIAddEdge) {
        edgeArray.push(edge);
        console.log(edgeArray);
        rysujGraf();
    } else {
        alert('Krawędź ' + document.getElementById('idK').value + ' już istnieje');
    }



});

function rysujGraf() {
    var graph = {};
    graph["nodes"] = nodesArray;
    graph["edges"] = edgeArray;

    document.getElementById('graph').innerHTML = "";

    s = new sigma({
        graph: graph,
        renderer: {
            container: document.getElementById('graph'),
            type: 'canvas'
        },
        settings: {
            doubleClickEnabled: false,
            minEdgeSize: 0.5,
            maxEdgeSize: 4,
            enableEdgeHovering: true,
            edgeHoverColor: 'edge',
            defaultEdgeHoverColor: '#000',
            edgeHoverSizeRatio: 1,
            edgeHoverExtremities: true,
            edgeColor: 'default',
            defaultEdgeColor: '#D62C1A'
        }
    });
}

document.getElementById('saveFile').addEventListener('click', function () {

    resultFile.nodes = nodesArray;
    resultFile.edges = edgeArray;

    var blob = new Blob([JSON.stringify(resultFile)], {
        type: "text/json;charset=utf-8"
    });

    saveAs(blob, "graph.json");
});

var fileInput = document.getElementById('inputFile');

fileInput.addEventListener('change', function (e) {
    var file = fileInput.files[0];
    console.log(file);

    var reader = new FileReader();

    reader.onload = function (e) {
        nodesArray = reader.result;
        console.log(nodesArray);
        var res = JSON.parse(nodesArray);

        nodesArray = res.nodes;
        edgeArray = res.edges;
        rysujGraf();
    }

    reader.readAsText(file);
});
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
document.getElementById('animate').addEventListener('click', function () {
     var nodesToAnimate = [];


    for (var i = 0; i < s.graph.nodes().length; i++) {
        s.graph.nodes()[i].color = getRandomColor();
        s.graph.nodes()[i].hover_color = getRandomColor();
        s.graph.nodes()[i].toX = Math.floor(Math.random() * 100);
        s.graph.nodes()[i].toY = Math.floor(Math.random() * 100);
        s.graph.nodes()[i].sizeW = Math.floor(Math.random() * 10);

        nodesToAnimate.push(s.graph.nodes()[i].id);
    }

    sigma.plugins.animate(
        s, {
            x: 'toX',
            y: 'toY',
            size: 'sizeW',
            color: 'hover_color'
        }, {
            nodes: nodesToAnimate,
            easing: 'cubicInOut',
            duration: 300,
            onComplete: function () {
                // do stuff here after animation is complete
            }
        }
    );





})
